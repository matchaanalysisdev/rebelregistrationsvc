﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Register.Rebeld.BaseTypes
{
    public class Rebeld
    {
        public string Name { get; set; }
        public string Planet { get; set; }
        public DateTime Registered { get; set; }
    }
}
