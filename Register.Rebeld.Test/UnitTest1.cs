﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Register.Rebeld.Write.Interfaces;
using System.Collections.Generic;
using Register.Rebeld.Contracts.DataContratcts;
using Register.Rebeld.Service;

namespace Register.Rebeld.Test
{
    [TestClass]
    public class UnitTest1
    {
        private Mock<IWriteToTxt> _writeToTxt;

        [TestInitialize]
        public void init()
        {
            _writeToTxt = new Mock<IWriteToTxt>();
            _writeToTxt.Setup(obj => obj.WriteRebelds(It.IsAny<List<RebeldData>>())).Returns(true);
        }
        [TestMethod]
        public void Write_To_File_OK()
        {
            //Arrange
            var rebeld = new RegisterRebeld(_writeToTxt.Object);
            //Act
            var list = new List<RebeldData>();
            var test = rebeld.RegisterRebeldBatch(list);
            //Assert
            Assert.IsTrue(test);
        }
    }
}
