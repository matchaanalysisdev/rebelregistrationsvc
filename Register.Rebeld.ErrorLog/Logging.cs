﻿using Register.Rebeld.ErrorLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;

namespace Register.Rebeld.ErrorLog
{
    public class Logging : ILogging
    {
        private string _basePath;
        public Logging(string basePath)
        {
            _basePath = basePath;
        }
        public void WriteLog(string pyramid, string message)
        {
            try
            {
                using (StreamWriter outputFile = new StreamWriter(string.Format("{0}/Logs/ApplicationErrors.txt", _basePath), true))
                {
                    outputFile.WriteLine(string.Format("{0} - An error ocurred at Imperial Citizen Registration: {1}, Error Message: {2}",
                        DateTime.Now.ToString(), pyramid, message));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(string.Format("Logger Failed Message: {0}", ex.Message));
            }
        }
    }
}