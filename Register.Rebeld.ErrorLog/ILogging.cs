﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Register.Rebeld.ErrorLog
{
    public interface ILogging
    {
        void WriteLog(string pyramid, string message);
    }
}
