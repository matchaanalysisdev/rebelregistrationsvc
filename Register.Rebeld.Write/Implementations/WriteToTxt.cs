﻿using Register.Rebeld.Write.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Register.Rebeld.Contracts.DataContratcts;
using System.IO;
using Register.Rebeld.ErrorLog;

namespace Register.Rebeld.Write.Implementations
{
    public class WriteToTxt : IWriteToTxt
    {
        private ILogging _errorLog { get; set; }

        private string _basePath;
        public WriteToTxt(string basePath)
        {
            _basePath = basePath;
            _errorLog = new Logging(basePath);
        }
        public bool WriteRebelds(List<RebeldData> listRebeld)
        {
            try
            {
                using (StreamWriter outputFile = new StreamWriter(string.Format("{0}/Rebelds/RebeldRegistration.txt", _basePath), true))
                {
                    foreach (var rebeld in listRebeld)
                    {
                        var newRebeld = new BaseTypes.Rebeld()
                        {
                            Name = rebeld.Name,
                            Planet = rebeld.Planet,
                            Registered = DateTime.Now
                        };
                        outputFile.WriteLine(string.Format("Registration Date: {0}, Rebeld Name: {1}, Planet: {2}", newRebeld.Registered.ToShortDateString(), newRebeld.Name, newRebeld.Planet));
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                _errorLog.WriteLog("Writting Rebeld to file failed", ex.Message);
                return false;
            }
        }
    }
}
