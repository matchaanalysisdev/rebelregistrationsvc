﻿using Register.Rebeld.Contracts.DataContratcts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Register.Rebeld.Write.Interfaces
{
    public interface IWriteToTxt
    {
        bool WriteRebelds(List<RebeldData> listRebeld);
    }
}
