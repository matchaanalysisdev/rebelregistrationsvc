﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Register.Rebeld.Contracts.DataContratcts
{
    [DataContract]
    public class RebeldData
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Planet { get; set; }
    }
}
