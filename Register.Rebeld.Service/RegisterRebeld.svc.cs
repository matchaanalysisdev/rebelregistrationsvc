﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Register.Rebeld.Contracts.DataContratcts;
using Register.Rebeld.Write.Interfaces;
using Ninject;
using Register.Rebeld.ErrorLog;

namespace Register.Rebeld.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class RegisterRebeld : IRegisterRebeld
    {
        [Inject]
        public IWriteToTxt writeToTxt { get; set; }

        [Inject]
        public ILogging errorLog { get; set; }

        public RegisterRebeld()
        {

        }

        public RegisterRebeld(IWriteToTxt writeMock)
        {
            writeToTxt = writeMock;
        }

        public bool RegisterRebeldBatch(IEnumerable<RebeldData> listRebelds)
        {
            try
            {
                return writeToTxt.WriteRebelds(listRebelds.ToList());
            }
            catch (Exception ex)
            {
                errorLog.WriteLog("Registering Rebeld List Failed", ex.Message);
                return false;
            }
        }
    }
}
