﻿using Register.Rebeld.Contracts.DataContratcts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Register.Rebeld.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IRegisterRebeld
    {
        [OperationContract]
        bool RegisterRebeldBatch(IEnumerable<RebeldData> listRebelds);
    }
}
